import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, Image } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
// import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';


export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)

    if(this.state.password === "12345678"){
      console.log(this.state.password)
      this.setState({isError: false})
      this.props.navigation.navigate('Main', { screen: 'Home'})
    }else {
      this.setState({isError: true})
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image source={require('../assets/undraw_medical_research_qg4d-2.svg')} style = {{ height: 100, resizeMode: 'contain' } }></Image>
          <Text style={styles.titleText}>Info Corona</Text>
          
        </View>

        <View style={styles.formContainer}>
        

        <Input
          placeholder='Username/Email'
          style = {styles.textInput}
          
          onChangeText={userName => this.setState({ userName })}
          leftIcon={
            <Icon
              name='user-circle'
              size={24}
              color='#1890F7'
            />
          }
        />
          <Input
          placeholder='Password'
          style = {styles.textInput}
          onChangeText={password => this.setState({ password })}
          leftIcon={
            <Icon
              name='key'
              size={24}
              color='#1890F7'
            />
          }
        />
        
         
          
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
          <Button title='Login' onPress={() => this.loginHandler()} />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#1890F7',
    textAlign: 'center',
    margin: 20,
  },
  subTitleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'blue',
    alignSelf: 'flex-end',
    marginBottom: 16
  },
  formContainer: {
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  labelText: {
    fontWeight: 'bold'
  },
  textInput: {
    width: 500,
    backgroundColor: 'white'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});
