import React, { Component } from 'react';

import { StyleSheet, Platform, View, Text, TouchableOpacity, Alert, Image } from 'react-native';

import PropTypes from 'prop-types';

import axios from 'axios';


export default class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            categories: []
        };
      }
      componentDidMount() {

        axios.get(`https://api.kawalcorona.com/indonesia`)
          .then(res => {
            const categories = res.data;
            console.log(categories);
            this.setState({ categories });
          })
      }

  buttonClick = () => {

    this.props.navigation.navigate('Detail')

  }

  render() {

    const {categories} = this.state;

    console.log(categories)

    return (

      <View style={styles.MainContainer}>

        <View style={styles.priceCard_Container}>

            
                <Text style={[styles.title, { color: '#588da8' }]}> Positif </Text>
                { 
                categories.map(user => 
                <Text style={[styles.price, { color: '#000' }]}>{user.positif}</Text>
                )}
            
                <Text style={[styles.title, { color: '#14b1ab' }]}> Sembuh </Text>
                { 
                categories.map(user => 
                <Text style={[styles.price, { color: '#000' }]}>{user.sembuh}</Text>
                )}
            
                <Text style={[styles.title, { color: '#d8345f' }]}> Meninggal </Text>
                { 
                categories.map(user => 
                <Text style={[styles.price, { color: '#000' }]}>{user.meninggal}</Text>
                )}

                
           
            <TouchableOpacity onPress={this.buttonClick} activeOpacity={0.4} style={styles.price_Button} >

                <Image
                    source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
                    style={styles.iconStyle} />

                <Text style={styles.TextStyle}> Detail </Text>

            </TouchableOpacity>

        </View>

      </View>

    );
  }
}


const styles = StyleSheet.create({

  MainContainer: {

    flex: 1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',

  },

  priceCard_Container: {

    alignItems: 'center',
    justifyContent: 'center',
    width: '85%',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#78909C',
    padding: 15

  },

  title: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },

  price: {
    fontSize: 29,
    fontWeight: 'bold',
  },

  price_Info: {
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 5,
    color: '#B0BEC5'
  },

  price_Button: {

    width: '90%',
    marginTop: 15,
    marginBottom: 10,
    backgroundColor: '#1890F7',
    borderRadius: 4,
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'center',

  },

  iconStyle: {

    width: 25,
    height: 25,
    justifyContent: 'flex-start',
    alignItems: 'center',
    tintColor: '#fff'

  },

  TextStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 15
  }


});